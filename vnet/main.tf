terraform {
  required_version = ">= 0.14"
}

provider "azurerm" {
  version = ">=2.51"
  features {}
}

resource "azurerm_resource_group" "rg" {
  name = "tumitest-rg"
  location = "eastus"
}

resource "azurerm_network_security_group" "nsg" {
  name                = "mynsg"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  security_rule {
    name                       = "test123"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

resource "azurerm_virtual_network" "VirtualNetwork" {
  name                = "my-vnet"
  address_space       = ["10.1.0.0/24"]
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
}

resource "azurerm_subnet" "Subnet" {
  name                 = "Subnet01"
  resource_group_name  = azurerm_resource_group.rg.name
  virtual_network_name = "my-vnet"
  address_prefixes     = ["10.1.0.0/26"]
}

resource "azurerm_network_interface" "NIC" {
  name                = "vmNIC"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name

  ip_configuration {
    name                          = "IP"
    subnet_id                     = azurerm_subnet.Subnet.id
    private_ip_address_allocation = "Dynamic"
  }
}