terraform {
  required_version = ">= 0.14"
}

provider "azurerm" {
  version = ">=2.51"
  features {}
}

data "azurerm_subnet" "Subnet" {
  name                 = "Subnet01"
  virtual_network_name = "my-vnet"
  resource_group_name  = "tumitest-rg"
}

module "az_vm" {
  source                        = "git::https://gitlab.com/tunmise417/az_vm"
  resource_group_name           = "tumitest-rg"
  vm_hostname                   = "azwus2w11ent"
  is_windows_image              = true
  admin_password                = "Whyclef@1992"
  nb_instances                  = 1
  vm_os_publisher               = "microsoftwindowsdesktop"
  vm_os_offer                   = "windows-11"
  vm_os_sku                     = "win11-21h2-ent"
  vm_size                       = "Standard_B4ms"
  vnet_subnet_id                = data.azurerm_subnet.Subnet.id
  boot_diagnostics              = true
  delete_os_disk_on_termination = false

  extra_disks = [
    {
      size = 100
      name = "data"
    }
  ]
}